import Sequelize, { Model } from 'sequelize';

class Comment extends Model {
  static init(sequelize) {
    super.init(
      {
        contentText: Sequelize.STRING
      },
      {
        sequelize,
        tableName: 'comments',
        freezeTableName: true
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'userId', as: 'user' });
    this.belongsTo(models.Post, { foreignKey: 'postId', as: 'post' });
  }
}

export default Comment;
