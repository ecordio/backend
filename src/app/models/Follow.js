import Sequelize, { Model } from 'sequelize';
import bcript from 'bcryptjs';

class Follow extends Model {
  static init(sequelize) {
    super.init(
      {},
      {
        sequelize,
        tableName: 'follows',
        freezeTableName: true
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'followId', as: 'following' });
    this.belongsTo(models.User, { foreignKey: 'followerId', as: 'follower' });
  }
}

export default Follow;
