import Sequelize, { Model } from 'sequelize';

class PostFile extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        path: Sequelize.STRING,
        type: Sequelize.STRING
      },
      {
        sequelize,
        tableName: 'postfiles',
        freezeTableName: true
      }
    );
    return this;
  }
}

export default PostFile;
