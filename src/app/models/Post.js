import Sequelize, { Model } from 'sequelize';

class Post extends Model {
  static init(sequelize) {
    super.init(
      {
        title: Sequelize.STRING,
        contentText: Sequelize.STRING,
        postType: Sequelize.STRING
      },
      {
        sequelize,
        tableName: 'posts',
        freezeTableName: true
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'userId', as: 'user' });
    this.hasMany(models.PostFile, { foreignKey: 'postId', as: 'file' });
    this.hasMany(models.Comment, { foreignKey: 'postId', as: 'comment' });
  }
}

export default Post;
