import Post from '../models/Post';
import User from '../models/User';
import File from '../models/File';
import PostFile from '../models/PostFile';
import Comment from '../models/Comment';
import { Sequelize, Op } from 'sequelize';

class SearchControloler {
  async searchUsers(req, res) {
    const searchText = req.query.searchText;
    
    const resultSearch = await User.findAll({
      where: { name: { [Op.iLike]: '%' + searchText + '%' } },
      attributes: ['name', 'id', 'lattes_link', 'email', 'participation_type', 'entry_date'],
      include: [
        { model: File, as: 'file', attributes: ['id', 'path'] }
      ]
    });

    if (!resultSearch) {
      return res.json({ 200: 'no results found', list: [] });
    }

    return res.json({ list: resultSearch });
  }

  async searchPost(req, res) {
    const searchText = req.query.searchText;

    const resultSearch = await Post.findAll({
      where: { title: { [Op.iLike]: '%' + searchText + '%' } },
      sort: ['"updatedAt", DESC'],
      attributes: ['id', 'title', 'contentText'],
      include: [
        {
          model: User,
          as: 'user',
          attributes: ['name', 'id', 'lattes_link', 'email', 'participation_type', 'entry_date'],
          include: [
            { model: File, as: 'file', attributes: ['id', 'path'] }
          ]
        },
        {
          model: Comment,
          as: 'comment',
          attributes: ['id', 'contentText'],
          include: [
            {
              model: User,
              as: 'user',
              attributes: ['name', 'id', 'lattes_link', 'email', 'participation_type', 'entry_date'],
              include: [
                { model: File, as: 'file', attributes: ['id', 'path'] }
              ]
            }
          ]
        },
        {
          model: PostFile,
          as: 'file',
          attributes: ['id','name', 'path','type']
        }
      ]
    });

    if (!resultSearch) {
      return res.json({ 200: 'no results found', list: [] });
    }

    return res.json({ list: resultSearch });
  }
}

export default new SearchControloler();
