import User from '../models/User';
import File from '../models/File';
const Yup = require('yup');

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string()
        .required()
        .min(6),
      participation_type: Yup.string().required(),
      entry_date: Yup.date().required(),
      lattes_link: Yup.string().required(),
      phone: Yup.string().required()
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(415).json({ userMessage: 'validation fails' });
    }

    const userExists = await User.findOne({
      where: { email: req.body.email }
    });

    if (userExists) {
      return res.status(400).json({ userMessage: 'user already exists.' });
    }

    const { id, name, email, lattes_link } = await User.create(req.body).catch(
      err => {
        return res
          .status(415)
          .json({ userMessage: 'validation fails', errMessage: err });
      }
    );

    return res.json({
      id,
      name,
      email,
      lattes_link
    });
  }

  async getUser(req, res) {    
    const user = await User.findOne({
      where: { id: req.userId },
      attributes: [
        'name',
        'email',
        'lattes_link',
        'participation_type',
        'entry_date',
        'phone'
      ],
      include: [
        { model: File, as: 'file', attributes: ['id', 'path'] }
      ]
    });    

    return res.json(user);
  }

  async update(req, res) {    
    const schema = Yup.object().shape({
      name: Yup.string(),
      email: Yup.string().email(),
      lattes_link: Yup.string(),
      oldPassword: Yup.string().notRequired(),
      entry_date: Yup.date(),
      participation_type: Yup.string(),
      phone: Yup.string(),
      password: Yup.string()
        .min(6)
        .when('oldPassword', (oldPassword, field) =>
          oldPassword ? field.required() : field
        ),
      confirmPassword: Yup.string().when('password', (password, field) =>
        password ? field.required().oneOf([Yup.ref('password')]) : field
      ),
    });
    const {email, oldPassword, entry_date} = req.body
  
    schema.validate(req.body).catch(error => {
      console.log("log do req body");
      console.log(error);      
    })

    if (!(await schema.isValid(req.body))) {
      return res.json({ error: 'validation fails' });
    }
    const user = await User.findByPk(req.userId);
    if (email !== user.email) {
      const userExists = await User.findOne({
        where: { email }
      });
      if (userExists) {
        return res.status(400).json({ error: 'user already exists.' });
      }
    }

    if (oldPassword && !(await user.checkPassword(oldPassword))) {
      return res.status(401).json({ error: 'password does not match ' });
    }
    
    const updatedUser = await user.update(req.body);

    return res.json({
      updatedUser
    });
  }
}

export default new UserController();
