import Comment from '../models/Comment';
import User from '../models/User';
import File from '../models/File';
class CommentController {
  async store(req, res) {
    const {userId} = req
    const { postId,  contentText } = req.body;
    const comment = await Comment.create({
      postId,
      userId,
      contentText
    });
    const fullComment = await Comment.findOne({
      where: {id: comment.dataValues.id},
      include: [
        {
          model: User,
          as: 'user',
          attributes: ['name', 'id', 'lattes_link', 'email', 'participation_type', 'entry_date'],
          include: [
            { model: File, as: 'file', attributes: ['id', 'path'] }
          ]
        }
      ]
    })

    return res.json(fullComment);
  }

  async index(req, res) {
    const { postId } = req.body;
    const comments = await Comment.findAll({
      where: { postId },
      as: 'comment',
      attributes: ['userId', 'contentText'],
      sort: ['updatedAt']
    });

    if (!comments) {
      return res.json({ 200: [] });
    }

    return res.json({ 200: comments });
  }

  async delete(req, res) {
    const comment = await Comment.findOne({
      where: {
        id: req.body.commentId
      }
    });

    if (comment) {
      comment.destroy();
    } else {
      return res.json({
        400: 'comment not provided'
      });
    }

    return res.json({ response: 200 });
  }
}

export default new CommentController();
