import Post from '../models/Post';
const Yup = require('yup');
import PostFile from '../models/PostFile';
import User from '../models/User';
import File from '../models/File';
import Comment from '../models/Comment';

class PostController {
  async store(req, res) {
    const schema = Yup.object().shape({
      title: Yup.string().required()
    });
    
    if (!(await schema.isValid(req.body))) {
      return res.json({ error: 'validation fails' });
    }

    const post = await Post.create({
      title: req.body.title,
      contentText: req.body.contentText,
      userId: req.userId,
      postType: req.body.postType
    });
    
    return res.json({
      post
    });
  }

  async myPosts(req, res) {
    const user = await User.findOne({
      where: { id: req.query.userId },
      attributes: ['name', 'id', 'lattes_link', 'email', 'participation_type', 'entry_date', 'phone'],
      include: [
        { model: File, 
          as: 'file', 
          attributes: ['id', 'path'] 
        }
      ]
    })

    const posts = await Post.findAll({
      where: { userId: req.query.userId },
      sort: ['"updatedAt", DESC'],
      attributes: ['id', 'title', 'contentText'],
      include: [
        {
          model: User,
          as: 'user',
          attributes: ['name', 'id', 'lattes_link', 'email', 'participation_type', 'entry_date'],
          include: [
            { model: File, as: 'file', attributes: ['id', 'path'] }
          ]
        },
        {
          model: Comment,
          as: 'comment',
          attributes: ['id', 'contentText'],
          include: [
            {
              model: User,
              as: 'user',
              attributes: ['name', 'id', 'lattes_link', 'email', 'participation_type', 'entry_date'],
              include: [
                { model: File, as: 'file', attributes: ['id', 'path'] }
              ]
            }
          ]
        },
        {
          model: PostFile,
          as: 'file',
          attributes: ['id','name', 'path','type']
        }
      ]
    });

    return res.json({ list: posts, user: user });
  }
 

  async index(req, res) {
    var postType = req.query.postType
    const subconWhere = postType !== "all" ? {postType} : {}
    console.log(subconWhere);
    
    var posts = await Post.findAll({
        subconWhere,
        sort: ['"updatedAt", DESC'],
        attributes: ['id', 'title', 'contentText', 'postType'],
        include: [
          {
            model: User,
            as: 'user',
            attributes: ['name', 'id', 'lattes_link', 'email', 'participation_type', 'entry_date'],
            include: [
              { model: File, as: 'file', attributes: ['id', 'path'] }
            ]
          },
          {
            model: Comment,
            as: 'comment',
            attributes: ['id', 'contentText'],
            include: [
              {
                model: User,
                as: 'user',
                attributes: ['name', 'id', 'lattes_link', 'email', 'participation_type', 'entry_date'],
                include: [
                  { model: File, as: 'file', attributes: ['id', 'path'] }
                ]
              }
            ]
          },
          {
            model: PostFile,
            as: 'file',
            attributes: ['id','name', 'path','type']
          }
        ]
      });
    return res.json({ posts: posts });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      title: Yup.string(),
      contentText: Yup.string().email()
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({ error: 'validation fails' });
    }

    const post = await Post.findByPk(req.postId);
    if (req.userId !== post.userId) {
      return res.json({ 400: 'you are not allowed to change this post' });
    }

    return res.json({
      post
    });
  }

  async delete(req, res) {
    const { post } = req.body;

    const response = await Post.delete(post);

    return res.json({ 200: 'ok', response });
  }
}

export default new PostController();
