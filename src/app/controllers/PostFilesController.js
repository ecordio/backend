import PostFile from '../models/PostFile';
class PostFilesController {
  async store(req, res) {                  
    const { postId } = req.body
    const {originalname: name, location, mimetype } = req.file;

    const file = await PostFile.create({
      name,
      postId,
      path: location,
      type: mimetype
    });    
    return res.json(file);
  }
}

export default new PostFilesController();
  