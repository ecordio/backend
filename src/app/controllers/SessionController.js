import jwt from 'jsonwebtoken';
import User from '../models/User';
const Yup = require('yup');
import auth from '../../config/auth';

class SessionController {
  async store(req, res) {    
    const schema = Yup.object().shape({
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string()
        .required()
        .min(6)
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ userMessage: 'Ocorreu um erro. Verifique suas informações e tente novamente' });
    }

    const { email, password } = req.body;
    const user = await User.findOne({ where: { email } });

    if (!user) {
      return res.status(401).json({ userMessage: 'Usuário não encontrado!' });
    }

    if (!(await user.checkPassword(password))) {
      return res.status(401).json({ userMessage: 'Senha incorreta!' });
    }

    const { id, name, participation_type, entry_date } = user;

    return res.json({
      user: {
        id,
        name,
        email,
        participation_type,
        entry_date
      },
      token: jwt.sign({ id }, auth.secret, {
        expiresIn: auth.expiresIn
      }),
      expiresIn: auth.expiresIn
    });
  }
}

export default new SessionController();
