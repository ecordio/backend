import Follow from '../models/Follow';

class FollowController {
  async store(req, res) {
    const { followId, followerId } = req.body;
    const follow = await Follow.create({
      followId,
      followerId
    });

    return res.json(follow);
  }

  async delete(req, res) {
    const follow = await Follow.findOne({
      where: { followerId: req.body.userId, followId: req.body.followingId }
    });

    if (follow) {
      follow.destroy();
    } else {
      return res.json({
        400: 'follow not provided'
      });
    }

    return res.json({ response: 200 });
  }
}

export default new FollowController();
