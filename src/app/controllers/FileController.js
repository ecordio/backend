import File from '../models/File';
class FileController {
  async store(req, res) {   
    const { originalname: name, location } = req.file;
    const user = JSON.parse(req.userId)
    const file = await File.create({
      name,
      path: location.toString(),
      userId: user
    });

    return res.json(file);
  }
}

export default new FileController();
