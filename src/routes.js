import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';
import UserController from './app/controllers/UserController';
import authMiddleware from './app/middlewares/auth';
import SessionController from './app/controllers/SessionController';
import FileController from './app/controllers/FileController';
import FollowController from './app/controllers/FollowController';
import PostController from './app/controllers/PostController';
import PostFilesController from './app/controllers/PostFilesController';
import CommentController from './app/controllers/CommentController';
import SearchController from './app/controllers/SearchController';

const routes = new Router();
const upload = multer(multerConfig);
routes.post('/users', UserController.store);
routes.post('/sessions', SessionController.store);

routes.use(authMiddleware);

routes.put('/users', UserController.update);
routes.post('/files', upload.single('file'), FileController.store);

routes.get('/profile', UserController.getUser);
routes.get('/myposts', PostController.myPosts);

routes.post('/follow', FollowController.store);
routes.delete('/unfollow', FollowController.delete);

routes.get('/post', PostController.index);
routes.post('/postfile', upload.single('file'), PostFilesController.store);
routes.post('/post', PostController.store);
routes.put('/post', PostController.update);
routes.delete('/post', PostController.delete);

routes.post('/commentPost', CommentController.store);
routes.get('/comments', CommentController.index);
routes.delete('/comments', CommentController.delete);

routes.get('/searchPost', SearchController.searchPost);
routes.get('/searchUser', SearchController.searchUsers);

export default routes;
