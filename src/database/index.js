import Sequelize from 'sequelize';
import databaseConfig from '../config/database';
import User from '../app/models/User';
import File from '../app/models/File';
import Follow from '../app/models/Follow';
import Post from '../app/models/Post';
import PostFile from '../app/models/PostFile';
import Comment from '../app/models/Comment';

const models = [User, Post, File, Follow, PostFile, Comment];
class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);
    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
